  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PAS</title>
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
    </head>



<?php

if(isset($_POST['company_add']))
{  
     
    
        require('mysqli_connect.php');
        $sql = "INSERT INTO Company (com_id,c_name,cgpa,descptn,depmt,location,package,interests) VALUES (?,?,?,?,?,?,?,?)"; 
        $st = $dbc->prepare($sql);
        $st->bind_param('ssdsssds',trim($_POST['com_id']),trim($_POST['c_name']),trim($_POST['cgpa']),trim($_POST['descptn']),trim($_POST['depmt']),trim($_POST['location']),trim($_POST['package']),trim($_POST['interests']));

        $st->execute();

        // $message = "Student added";
        //   echo "<script>alert('".$message."'); window.location.href='/cs315/add_stud_from_admin.php';</script>";

        $st->close();
        



        $sql2 = "INSERT INTO CLogin (com_id,passwrd) VALUES (?,?)"; 
        $st2 = $dbc->prepare($sql2);
        $st2->bind_param('ss',trim($_POST['com_id']),trim($_POST['passwrd']));
        $st2->execute();

        $message = "Company added";
          echo "<script>alert('".$message."'); window.location.href='/cs315/admin_home.php';</script>";

        $st->close();


}

?>


    <body>
      <div class = "part1">
      <form action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']); 
            ?>" method="post">
    

        <fieldset>

          <legend>Set Username and Password</legend>
          <label for="mail">Username (use company id.):</label>
          <input type="text" id="com_id" name="com_id" >

          <label for="name">Password:</label>
          <input type="password" id="passwrd" name="passwrd">

          <legend>Fill additional attributes</legend>
          <legend><span class="number">1</span>Company Information</legend>

          <label for="name">Company Name:</label>
          <input type="text" id="c_name" name="c_name" >
          
          <label for="mail">Description:</label>
          <input type="text" id="descptn" name="descptn" >

          <label for="mail">Location :</label>
          <input type="text" id="location" name="location">
          
          <label for="mail">Package in LPA:</label>
          <input type="text" id="package" name="package">

          <legend><span class="number">2</span>Company Requirements</legend>  

          <label for="mail">Intersts:</label>
          <input type="text" id="Interests" name="interests">

          <label for="mail">CGPA:</label>
          <input type="text" id="cgpa" name="cgpa">

          <label for="mail">Department:</label>
          <input type="text" id="depmt" name="depmt">

        <!--   <label>Age:</label>
          <input type="radio" id="under_13" value="under_13" name="user_age"><label for="under_13" class="light">Under 13</label><br>
          <input type="radio" id="over_13" value="over_13" name="user_age"><label for="over_13" class="light">13 or older</label> -->
        </fieldset>
        <button type='submit' name='company_add'>Add Company</button> 
        <!-- <button type="submit" name="Update_info">Update</button> -->
      </form>
      </div>
    </body>
</html>