<?php
require('admin_tester.php');
?>



<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Responsive CSS Tabs</title>
   
    
    
        <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
@import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html, body {
  height: 100%;
}

body {
  font: 14px/1 'Open Sans', sans-serif;
  color: #555;
  background: #eee;
}

h1 {
  padding: 50px 0;
  font-weight: 400;
  text-align: center;
}

p {
  margin: 0 0 20px;
  line-height: 1.5;
}

main {
  min-width: 320px;
  max-width: 90%;
  padding: 25px;
  margin: 0 auto;
  background: #fff;
}

section {
  display: none;
  padding: 20px 0 0;
  border-top: 1px solid #ddd;
}

input {
  display: none;
}

label {
  display: inline-block;
  margin: 0 0 -1px;
  padding: 15px 20px;
  font-weight: 600;
  text-align: center;
  color: #bbb;
  border: 1px solid transparent;
}

a {
    text-decoration: none;
}


.w3-card2{
  width: 90%;
   margin-bottom: 1%;
   
   margin-left: 2%;
  padding-top: 3%;
  padding-left: 3%;
  padding-right: 3%;
  padding-bottom: 2%;
  box-shadow:0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important
}


#content7{

margin-left: 10%;


}




#left, #middle, #right {display: inline-block; *display: inline; zoom: 1; }
#left {width: 30%; }
#middle {width: 15%;}
#right {width: 20%; }

label:before {
  font-family: fontawesome;
  font-weight: normal;
  margin-right: 4px;
}

label[for*='1']:before {
  content: '\f1cb';
}

label[for*='2']:before {
  content: '\f17d';
}

label[for*='3']:before {
  content: '\f16b';
}

label[for*='4']:before {
  content: '\f1a9';
}
label[for*='5']:before {
  content: '\231a';
}
label[for*='6']:before {
  content: '\301a';
}
label[for*='7']:before {
  content: '\281a';
}
label[for*='8']:before {
  content: '\276a';
}


label:hover {
  color: #888;
  cursor: pointer;
}

input:checked + label {
  color: #555;
  border: 1px solid #ddd;
  border-top: 2px solid orange;
  border-bottom: 1px solid #fff;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4,
#tab5:checked ~ #content5,
#tab6:checked ~ #content6,
#tab7:checked ~ #content7,
#tab8:checked ~ #content8{
  display: block;
}

@media screen and (max-width: 650px) {
  label {
    font-size: 0;
  }

  label:before {
    margin: 0;
    font-size: 16px;
  }
}
@media screen and (max-width: 400px) {
  label {
    padding: 15px;
  }
}


.part2 button {
  padding: 19px 39px 18px 39px;
  color: #FFF;
  background-color: #4bc970;
  font-size: 18px;
  text-align: center;
  font-style: normal;
  border-radius: 5px;
  width: 35%;
  border: 1px solid #3ac162;
  border-width: 1px 1px 3px;
  box-shadow: 0 -1px 0 rgba(255,255,255,0.1) inset;
  margin-bottom: 10px;
  margin-top: 1%;
  margin-left: 25%;


}



    </style>

    
        <script src="js/prefixfree.min.js"></script>

   

    
  </head>



 






  <body>

    <h1>Placement Automation System</h1>

<main>
  
  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1">Post</label>
    
  <input id="tab2" type="radio" name="tabs" >
  <label for="tab2">Edit Studt</label>
    
  <input id="tab3" type="radio" name="tabs" >
  <label for="tab3">Edit Company</label>
    
  <input id="tab4" type="radio" name="tabs" >
  <label for="tab4">Approve</label>


  <input id="tab5" type="radio" name="tabs" >
  <label for="tab5">Add Student</label>
    
  <input id="tab6" type="radio" name="tabs" >
  <label for="tab6">Add Company</label>
    

    <input id="tab7" type="radio" name="tabs" >
  <label for="tab7">Cal</label>

    <input id="tab8" type="radio" name="tabs" >
  <label for="tab8">Messages</label>

  <input id="tab9" type="submit" name="tabs" onclick="location.href='logout.php';" >
  <label for="tab9">Bye</label> 


<?php
    
    session_start();

  
?>
    
  <section id="content1">

   <?php include("post_admin.php"); ?>

  </section>
    
  <section id="content2">


<?php 


 
    include("view_allstud_from_admin.php");


 ?>


  </section>
    
  <section id="content3">
    
   
<?php 


 
    include("view_allcom_from_admin.php");


 ?>


  </section>
    
  <section id="content4">



<?php
  




  require('mysqli_connect.php');

  $st = $dbc->prepare("SELECT * FROM S_posts as S where S.app='1' order by dte");
  $st->execute();
  $st->bind_result($post,$app,$dte);
  // var_dump($st);

  while ($row = $st->fetch())
    {
      // echo $nm;

        echo "<div class='w3-card2'>";
        echo "<div id='left'>".$post."</div>";
        echo "<div id='left'>".$dte."</div>";
        echo "<div id='right'>"."<a href='approve.php?blue=true&cid=".urlencode($dte)."'>Approve</a>"."</div>";

        echo "</div>";
       




    }

  $st->close();
  $dbc->close();



?>





  </section>
    

<section id="content5">
<?php include("add_stud_from_admin.php");
?>
</section>


    <section id="content6">
    <?php 
    include("add_com_from_admin.php");?>
    </section>





    <section id="content7">
    
      <iframe src="https://calendar.google.com/calendar/embed?src=sdrsbg2b56l5r8f2fpd7q9rl68%40group.calendar.google.com&ctz=Asia/Calcutta" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

      <div class = "part2">
       <button type="submit" name="Update_info" target="_blank" onclick="location.href = 'http://calendar.google.com/calendar/';">Edit Calender</button>
      </div>
  </section>
    

    <section id="content8">
    

    <?php 

      require('mysqli_connect.php');

  $st = $dbc->prepare("SELECT * FROM A_posts as S order by dte desc");
  $st->execute();
  $st->bind_result($post,$client,$app,$dte);
  // var_dump($st);

  while ($row = $st->fetch())
    {
      // echo $nm;

        echo "<div class='w3-card2'>";
        echo "<div id='left'>".$post."</div>";
        echo "<div id='left'>".$dte."</div>";
        echo "<div id='left'>".$client."</div>";
        if($app == '1')
        {
        echo "<div id='right'>"."Student"."</div>";
        }
        else
        {
          echo "<div id='right'>"."Company"."</div>";
        }
        echo "</div>";
       




    }

  $st->close();
  $dbc->close();


    ?>




  </section>



</main>
    
    
    
    
    
  </body>
</html>