﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="PersonalInfo.aspx.vb" Inherits="PersonalInfo" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="background-position: center center; background-image: url('backgroundimages/background.jpg'); background-repeat: no-repeat; background-attachment: scroll;">
        <br />
        <table align="center" border="0" cellpadding="0" cellspacing="0" 
            width="100%" 
            
            
            
            style="background-image: url('backgroundimages/background.jpg'); font-family: 'comic Sans MS'; font-size: large; font-weight: 500; color: #000000; height: 640px;">
            <tr valign="top">
                <td class="p10px registration_h b large ttu" colspan="2" 
                    
                    style="font-family: 'comic Sans MS'; font-size: x-large; font-weight: 500; font-style: normal; height: 29px; color: #FFFFFF; background-color: #000000;">
                    <marquee behavior="" style="width: 699px">Personal Information</marquee></td>
            </tr>
            <tr valign="top">
                <td class="p5px10px bgf8f8f8 bdrB" colspan="2" 
                    
                    style="font-family: 'Times New Roman', Times, serif; font-size: x-large; font-style: normal; font-weight: bolder; background-color: #C0C0C0;">
                    <br />
                    This is where recruiters will contact you<br />
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'Times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                    <span class="red">
                    <br />
                    *</span> Your Name</td>
                <td class="p5px10px">
                    <br />
                    <asp:TextBox ID="txtyname" runat="server" Width="294px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtyname"
                  
                    
                        ErrorMessage=" * UserName Cannot Be Left Blank" Font-Size="Small"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'Times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                    <span class="red">
                    <br />
                    *</span> Gender
                </td>
                <td class="p5px10px">
                    <br />
                    <asp:RadioButton ID="rMale" runat="server" GroupName="r1" Text="   Male" 
                        Checked="True" />
                    &nbsp;&nbsp;
                    <asp:RadioButton ID="rfemale" runat="server" GroupName="r1" 
                        Text="   Female" />
                    
                    </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'Times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                    <span class="red">
                    <br />
                    *</span> Date Of Birth</td>
                <td class="p5px10px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                    ControlToValidate="txtdob" 
                        ErrorMessage=" * Cannot Be Left Blank " Font-Size="Small"></asp:RequiredFieldValidator>
                    <br />
                    <asp:TextBox ID="txtdob" runat="server" Width="187px"></asp:TextBox>
                    [ dd/mm/yyyy ] &nbsp;
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="txtdob"
                    ValidationExpression="^(((((0[1-9])|(1\d)|(2[0-8]))/((0[1-9])|(1[0-2])))|((31/((0[13578])|(1[02])))|((29|30)/((0[1,3-9])|(1[0-2])))))/((20[0-9][0-9]))|((((0[1-9])|(1\d)|(2[0-8]))/((0[1-9])|(1[0-2])))|((31/((0[13578])|(1[02])))|((29|30)/((0[1,3-9])|(1[0-2])))))/((19[0-9][0-9]))|(29/02/20(([02468][048])|([13579][26])))|(29/02/19(([02468][048])|([13579][26]))))$"
                        ErrorMessage=" * please enter proper date" Font-Size="Small"></asp:RegularExpressionValidator>
                                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                    <span class="red">
                    <br />
                    *</span> Country
                </td>
                <td class="p5px10px">
                    
                    <br />
                    <asp:DropDownList ID="COUNTRY" runat="server">
                        <asp:ListItem Value="Afghanistan"></asp:ListItem>
                        <asp:ListItem value="AL">Albania</asp:ListItem>
                        <asp:ListItem value="DZ">Algeria</asp:ListItem>
                        <asp:ListItem value="AS">American Samoa</asp:ListItem>
                        <asp:ListItem value="AD">Andorra</asp:ListItem>
                        <asp:ListItem value="AO">Angola</asp:ListItem>
                        <asp:ListItem value="AI">Anguilla</asp:ListItem>
                        <asp:ListItem value="AQ">Antarctica</asp:ListItem>
                        <asp:ListItem value="AG">Antigua and Barbuda</asp:ListItem>
                        <asp:ListItem value="AR">Argentina</asp:ListItem>
                        <asp:ListItem value="AM">Armenia</asp:ListItem>
                        <asp:ListItem value="AW">Aruba</asp:ListItem>
                        <asp:ListItem value="AU">Australia</asp:ListItem>
                        <asp:ListItem value="AT">Austria</asp:ListItem>
                        <asp:ListItem value="AZ">Azerbaijan</asp:ListItem>
                        <asp:ListItem value="BS">Bahamas</asp:ListItem>
                        <asp:ListItem value="BH">Bahrain</asp:ListItem>
                        <asp:ListItem value="BD">Bangladesh</asp:ListItem>
                        <asp:ListItem value="BB">Barbados</asp:ListItem>
                        <asp:ListItem value="BY">Belarus</asp:ListItem>
                        <asp:ListItem value="BE">Belgium</asp:ListItem>
                        <asp:ListItem value="BZ">Belize</asp:ListItem>
                        <asp:ListItem value="BJ">Benin</asp:ListItem>
                        <asp:ListItem value="BM">Bermuda</asp:ListItem>
                        <asp:ListItem value="BT">Bhutan</asp:ListItem>
                        <asp:ListItem value="BO">Bolivia</asp:ListItem>
                        <asp:ListItem value="BA">Bosnia and Herzegowina</asp:ListItem>
                        <asp:ListItem value="BW">Botswana</asp:ListItem>
                        <asp:ListItem value="BV">Bouvet Island</asp:ListItem>
                        <asp:ListItem value="BR">Brazil</asp:ListItem>
                        <asp:ListItem value="IO">British Indian Ocean Territory</asp:ListItem>
                        <asp:ListItem value="BN">Brunei Darussalam</asp:ListItem>
                        <asp:ListItem value="BG">Bulgaria</asp:ListItem>
                        <asp:ListItem value="BF">Burkina Faso</asp:ListItem>
                        <asp:ListItem value="BI">Burundi</asp:ListItem>
                        <asp:ListItem value="KH">Cambodia</asp:ListItem>
                        <asp:ListItem value="CM">Cameroon</asp:ListItem>
                        <asp:ListItem value="CA">Canada</asp:ListItem>
                        <asp:ListItem value="CV">Cape Verde</asp:ListItem>
                        <asp:ListItem value="KY">Cayman Islands</asp:ListItem>
                        <asp:ListItem value="CF">Central African Republic</asp:ListItem>
                        <asp:ListItem value="TD">Chad</asp:ListItem>
                        <asp:ListItem value="CL">Chile</asp:ListItem>
                        <asp:ListItem value="CN">China</asp:ListItem>
                        <asp:ListItem value="CX">Christmas Island</asp:ListItem>
                        <asp:ListItem value="CC">Cocos (Keeling) Islands</asp:ListItem>
                        <asp:ListItem value="CO">Colombia</asp:ListItem>
                        <asp:ListItem value="KM">Comoros</asp:ListItem>
                        <asp:ListItem value="CG">Congo</asp:ListItem>
                        <asp:ListItem value="ZR">Congo, The Democratic Republic Of The</asp:ListItem>
                        <asp:ListItem value="CK">Cook Islands</asp:ListItem>
                        <asp:ListItem value="CR">Costa Rica</asp:ListItem>
                        <asp:ListItem value="HR">Croatia (local name: Hrvatska)</asp:ListItem>
                        <asp:ListItem value="CU">Cuba</asp:ListItem>
                        <asp:ListItem value="CY">Cyprus</asp:ListItem>
                        <asp:ListItem value="CZ">Czech Republic</asp:ListItem>
                        <asp:ListItem value="DK">Denmark</asp:ListItem>
                        <asp:ListItem value="DJ">Djibouti</asp:ListItem>
                        <asp:ListItem value="DM">Dominica</asp:ListItem>
                        <asp:ListItem value="DO">Dominican Republic</asp:ListItem>
                        <asp:ListItem value="TP">East Timor</asp:ListItem>
                        <asp:ListItem value="EC">Ecuador</asp:ListItem>
                        <asp:ListItem value="EG">Egypt</asp:ListItem>
                        <asp:ListItem value="SV">El Salvador</asp:ListItem>
                        <asp:ListItem value="GQ">Equatorial Guinea</asp:ListItem>
                        <asp:ListItem value="ER">Eritrea</asp:ListItem>
                        <asp:ListItem value="EE">Estonia</asp:ListItem>
                        <asp:ListItem value="ET">Ethiopia</asp:ListItem>
                        <asp:ListItem value="FK">Falkland Islands (Malvinas)</asp:ListItem>
                        <asp:ListItem value="FO">Faroe Islands</asp:ListItem>
                        <asp:ListItem value="FJ">Fiji</asp:ListItem>
                        <asp:ListItem value="FI">Finland</asp:ListItem>
                        <asp:ListItem value="FR">France</asp:ListItem>
                        <asp:ListItem value="FX">France Metropolitan</asp:ListItem>
                        <asp:ListItem value="GF">French Guiana</asp:ListItem>
                        <asp:ListItem value="PF">French Polynesia</asp:ListItem>
                        <asp:ListItem value="TF">French Southern Territories</asp:ListItem>
                        <asp:ListItem value="GA">Gabon</asp:ListItem>
                        <asp:ListItem value="GM">Gambia</asp:ListItem>
                        <asp:ListItem value="GE">Georgia</asp:ListItem>
                        <asp:ListItem value="DE">Germany</asp:ListItem>
                        <asp:ListItem value="GH">Ghana</asp:ListItem>
                        <asp:ListItem value="GI">Gibraltar</asp:ListItem>
                        <asp:ListItem value="GR">Greece</asp:ListItem>
                        <asp:ListItem value="GL">Greenland</asp:ListItem>
                        <asp:ListItem value="GD">Grenada</asp:ListItem>
                        <asp:ListItem value="GP">Guadeloupe</asp:ListItem>
                        <asp:ListItem value="GU">Guam</asp:ListItem>
                        <asp:ListItem value="GT">Guatemala</asp:ListItem>
                        <asp:ListItem value="GN">Guinea</asp:ListItem>
                        <asp:ListItem value="GW">Guinea-Bissau</asp:ListItem>
                        <asp:ListItem value="GY">Guyana</asp:ListItem>
                        <asp:ListItem value="HT">Haiti</asp:ListItem>
                        <asp:ListItem value="HM">Heard and Mc Donald Islands</asp:ListItem>
                        <asp:ListItem value="HN">Honduras</asp:ListItem>
                        <asp:ListItem value="HK">Hong Kong</asp:ListItem>
                        <asp:ListItem value="HU">Hungary</asp:ListItem>
                        <asp:ListItem value="IS">Iceland</asp:ListItem>
                        <asp:ListItem value="IN">India</asp:ListItem>
                        <asp:ListItem value="ID">Indonesia</asp:ListItem>
                        <asp:ListItem value="IR">Iran (Islamic Republic of)</asp:ListItem>
                        <asp:ListItem value="IQ">Iraq</asp:ListItem>
                        <asp:ListItem value="IE">Ireland</asp:ListItem>
                        <asp:ListItem value="IL">Israel</asp:ListItem>
                        <asp:ListItem value="IT">Italy</asp:ListItem>
                        <asp:ListItem value="JM">Jamaica</asp:ListItem>
                        <asp:ListItem value="JP">Japan</asp:ListItem>
                        <asp:ListItem value="JO">Jordan</asp:ListItem>
                        <asp:ListItem value="KZ">Kazakhstan</asp:ListItem>
                        <asp:ListItem value="KE">Kenya</asp:ListItem>
                        <asp:ListItem value="KI">Kiribati</asp:ListItem>
                        <asp:ListItem value="KW">Kuwait</asp:ListItem>
                        <asp:ListItem value="KG">Kyrgyzstan</asp:ListItem>
                        <asp:ListItem value="LV">Latvia</asp:ListItem>
                        <asp:ListItem value="LB">Lebanon</asp:ListItem>
                        <asp:ListItem value="LS">Lesotho</asp:ListItem>
                        <asp:ListItem value="LR">Liberia</asp:ListItem>
                        <asp:ListItem value="LY">Libyan Arab Jamahiriya</asp:ListItem>
                        <asp:ListItem value="LI">Liechtenstein</asp:ListItem>
                        <asp:ListItem value="LT">Lithuania</asp:ListItem>
                        <asp:ListItem value="LU">Luxembourg</asp:ListItem>
                        <asp:ListItem value="MO">Macao</asp:ListItem>
                        <asp:ListItem value="MK">Macedonia</asp:ListItem>
                        <asp:ListItem value="MG">Madagascar</asp:ListItem>
                        <asp:ListItem value="MW">Malawi</asp:ListItem>
                        <asp:ListItem value="MY">Malaysia</asp:ListItem>
                        <asp:ListItem value="MV">Maldives</asp:ListItem>
                        <asp:ListItem value="ML">Mali</asp:ListItem>
                        <asp:ListItem value="MT">Malta</asp:ListItem>
                        <asp:ListItem value="MH">Marshall Islands</asp:ListItem>
                        <asp:ListItem value="MQ">Martinique</asp:ListItem>
                        <asp:ListItem value="MR">Mauritania</asp:ListItem>
                        <asp:ListItem value="MU">Mauritius</asp:ListItem>
                        <asp:ListItem value="YT">Mayotte</asp:ListItem>
                        <asp:ListItem value="MX">Mexico</asp:ListItem>
                        <asp:ListItem value="FM">Micronesia</asp:ListItem>
                        <asp:ListItem value="MD">Moldova</asp:ListItem>
                        <asp:ListItem value="MC">Monaco</asp:ListItem>
                        <asp:ListItem value="MN">Mongolia</asp:ListItem>
                        <asp:ListItem value="MS">Montserrat</asp:ListItem>
                        <asp:ListItem value="MA">Morocco</asp:ListItem>
                        <asp:ListItem value="MZ">Mozambique</asp:ListItem>
                        <asp:ListItem value="MM">Myanmar</asp:ListItem>
                        <asp:ListItem value="NA">Namibia</asp:ListItem>
                        <asp:ListItem value="NR">Nauru</asp:ListItem>
                        <asp:ListItem value="NP">Nepal</asp:ListItem>
                        <asp:ListItem value="NL">Netherlands</asp:ListItem>
                        <asp:ListItem value="AN">Netherlands Antilles</asp:ListItem>
                        <asp:ListItem value="NC">New Caledonia</asp:ListItem>
                        <asp:ListItem value="NZ">New Zealand</asp:ListItem>
                        <asp:ListItem value="NI">Nicaragua</asp:ListItem>
                        <asp:ListItem value="NE">Niger</asp:ListItem>
                        <asp:ListItem value="NG">Nigeria</asp:ListItem>
                        <asp:ListItem value="NU">Niue</asp:ListItem>
                        <asp:ListItem value="NF">Norfolk Island</asp:ListItem>
                        <asp:ListItem value="KP">North Korea</asp:ListItem>
                        <asp:ListItem value="MP">Northern Mariana Islands</asp:ListItem>
                        <asp:ListItem value="NO">Norway</asp:ListItem>
                        <asp:ListItem value="OM">Oman</asp:ListItem>
                        <asp:ListItem value="OT">Other Country</asp:ListItem>
                        <asp:ListItem value="PK">Pakistan</asp:ListItem>
                        <asp:ListItem value="PW">Palau</asp:ListItem>
                        <asp:ListItem value="PA">Panama</asp:ListItem>
                        <asp:ListItem value="PG">Papua New Guinea</asp:ListItem>
                        <asp:ListItem value="PY">Paraguay</asp:ListItem>
                        <asp:ListItem value="PE">Peru</asp:ListItem>
                        <asp:ListItem value="PH">Philippines</asp:ListItem>
                        <asp:ListItem value="PN">Pitcairn</asp:ListItem>
                        <asp:ListItem value="PL">Poland</asp:ListItem>
                        <asp:ListItem value="PT">Portugal</asp:ListItem>
                        <asp:ListItem value="PR">Puerto Rico</asp:ListItem>
                        <asp:ListItem value="QA">Qatar</asp:ListItem>
                        <asp:ListItem value="RE">Reunion</asp:ListItem>
                        <asp:ListItem value="RO">Romania</asp:ListItem>
                        <asp:ListItem value="RU">Russian Federation</asp:ListItem>
                        <asp:ListItem value="RW">Rwanda</asp:ListItem>
                        <asp:ListItem value="KN">Saint Kitts and Nevis</asp:ListItem>
                        <asp:ListItem value="LC">Saint Lucia</asp:ListItem>
                        <asp:ListItem value="VC">Saint Vincent and the Grenadines</asp:ListItem>
                        <asp:ListItem value="WS">Samoa</asp:ListItem>
                        <asp:ListItem value="SM">San Marino</asp:ListItem>
                        <asp:ListItem value="ST">Sao Tome and Principe</asp:ListItem>
                        <asp:ListItem value="SA">Saudi Arabia</asp:ListItem>
                        <asp:ListItem value="SN">Senegal</asp:ListItem>
                        <asp:ListItem value="SC">Seychelles</asp:ListItem>
                        <asp:ListItem value="SL">Sierra Leone</asp:ListItem>
                        <asp:ListItem value="SG">Singapore</asp:ListItem>
                        <asp:ListItem value="SK">Slovakia (Slovak Republic)</asp:ListItem>
                        <asp:ListItem value="SI">Slovenia</asp:ListItem>
                        <asp:ListItem value="SB">Solomon Islands</asp:ListItem>
                        <asp:ListItem value="SO">Somalia</asp:ListItem>
                        <asp:ListItem value="ZA">South Africa</asp:ListItem>
                        <asp:ListItem value="KR">South Korea</asp:ListItem>
                        <asp:ListItem value="ES">Spain</asp:ListItem>
                        <asp:ListItem value="LK">Sri Lanka</asp:ListItem>
                        <asp:ListItem value="SH">St. Helena</asp:ListItem>
                        <asp:ListItem value="PM">St. Pierre and Miquelon</asp:ListItem>
                        <asp:ListItem value="SD">Sudan</asp:ListItem>
                        <asp:ListItem value="SR">Suriname</asp:ListItem>
                        <asp:ListItem value="SJ">Svalbard and Jan Mayen Islands</asp:ListItem>
                        <asp:ListItem value="SZ">Swaziland</asp:ListItem>
                        <asp:ListItem value="SE">Sweden</asp:ListItem>
                        <asp:ListItem value="CH">Switzerland</asp:ListItem>
                        <asp:ListItem value="SY">Syrian Arab Republic</asp:ListItem>
                        <asp:ListItem value="TW">Taiwan</asp:ListItem>
                        <asp:ListItem value="TJ">Tajikistan</asp:ListItem>
                        <asp:ListItem value="TZ">Tanzania</asp:ListItem>
                        <asp:ListItem value="TH">Thailand</asp:ListItem>
                        <asp:ListItem value="TG">Togo</asp:ListItem>
                        <asp:ListItem value="TK">Tokelau</asp:ListItem>
                        <asp:ListItem value="TO">Tonga</asp:ListItem>
                        <asp:ListItem value="TT">Trinidad and Tobago</asp:ListItem>
                        <asp:ListItem value="TN">Tunisia</asp:ListItem>
                        <asp:ListItem value="TR">Turkey</asp:ListItem>
                        <asp:ListItem value="TM">Turkmenistan</asp:ListItem>
                        <asp:ListItem value="TC">Turks and Caicos Islands</asp:ListItem>
                        <asp:ListItem value="TV">Tuvalu</asp:ListItem>
                        <asp:ListItem value="UG">Uganda</asp:ListItem>
                        <asp:ListItem value="UA">Ukraine</asp:ListItem>
                        <asp:ListItem value="AE">United Arab Emirates</asp:ListItem>
                        <asp:ListItem value="UK">United Kingdom</asp:ListItem>
                        <asp:ListItem value="US">United States</asp:ListItem>
                        <asp:ListItem value="UM">United States Minor Outlying Islands</asp:ListItem>
                        <asp:ListItem value="UY">Uruguay</asp:ListItem>
                        <asp:ListItem value="UZ">Uzbekistan</asp:ListItem>
                        <asp:ListItem value="VU">Vanuatu</asp:ListItem>
                        <asp:ListItem value="VA">Vatican City State (Holy See)</asp:ListItem>
                        <asp:ListItem value="VE">Venezuela</asp:ListItem>
                        <asp:ListItem value="VN">Vietnam</asp:ListItem>
                        <asp:ListItem value="VG">Virgin Islands (British)</asp:ListItem>
                        <asp:ListItem value="VI">Virgin Islands (U.S.)</asp:ListItem>
                        <asp:ListItem value="WF">Wallis And Futuna Islands</asp:ListItem>
                        <asp:ListItem value="EH">Western Sahara</asp:ListItem>
                        <asp:ListItem value="YE">Yemen</asp:ListItem>
                        <asp:ListItem value="YU">Yugoslavia</asp:ListItem>
                        <asp:ListItem value="ZM">Zambia</asp:ListItem>
                        <asp:ListItem value="ZW">Zimbabwe</asp:ListItem>
                        
                        
                        
                        
                        
                        
                    </asp:DropDownList>
                   </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                    <span class="red">
                    <br />
                    *</span> State</td>
                <td class="p5px10px">
                    <div id="txtHint">
                        <br />
                        <asp:TextBox ID="txtstate" runat="server" Width="268px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtstate"
                            ErrorMessage=" *  State Cannot Be Left Blank" Font-Size="Small"></asp:RequiredFieldValidator>
                    </div>
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2">
                    <div id="indian_city">
                        <table width="100%">
                            <tr valign="top">
                                <td class="p5px10px ar" 
                                    style="width: 28%; font-family: 'times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                                    <br />
                                    * City</td>
                                <td class="p5px10px">
                                    <div id="txtHint0">
                                        <br />
                        <asp:TextBox ID="txtcity" runat="server" Width="266px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                        ControlToValidate="txtcity"
                                            ErrorMessage=" * City Cannot Be Left Blank" Font-Size="Small"></asp:RequiredFieldValidator>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal; height: 50px;">
                    <span class="red">
                    <br />
                    *</span> Street Address</td>
                <td class="p5px10px" style="height: 50px">
                    <br />
                    <asp:TextBox ID="txtstreetadd" runat="server" Width="342px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="txtstreetadd"
                        ErrorMessage=" * Please Enter StreetAddress" Font-Size="Small"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="width: 200px; font-family: 'times New Roman', Times, serif; font-size: large; font-weight: bolder; font-style: normal;">
                    <span class="red">
                    <br />
                    *</span> Mobile Number</td>
                <td class="p5px10px">
                    <br />
                    
                    <asp:TextBox ID="txtmobnum" runat="server" Height="23px" Width="183px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ControlToValidate="txtmobnum"
                    ValidationExpression="^((\\+91-?)|0)?[0-9]{10}$"
                        ErrorMessage=" * Invalid Mobile Number" Font-Size="Small"></asp:RegularExpressionValidator>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txtmobnum"
                        ErrorMessage=" * Mob No Cannot Be Left Blank" Font-Size="Small"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr id="landline" valign="top">
                <td class="p5px10px ar" style="width: 200px">
                    <br />
                    * Email Id</td>
                <td class="p5px10px">
                    <asp:TextBox ID="txtemailid" runat="server" 
                        
                        
                        
                        
                        style="z-index: 1; position: absolute; top: 761px; left: 485px; height: 20px; width: 242px;"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                   
                    <asp:RegularExpressionValidator 
                        ID="RegularExpressionValidator2" runat="server" 
                        ControlToValidate="txtemailid"
                        ValidationExpression=".*@.*\..*"
                        ErrorMessage=" *  Email address is invalid." Font-Size="Small"></asp:RegularExpressionValidator>
                    &nbsp;&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                        ControlToValidate="txtemailid" ErrorMessage=" * Email Id Be Left Blank" 
                        Font-Size="Small"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr valign="top">
                <td class="p10px" colspan="2">
                    <p class="bdrBlight">
                        <img src="PLACEMENT%20IMAGES/placement.jpg" 
                            style="margin-bottom: 0; background-repeat: repeat;" /><asp:Button ID="Button1" 
                            runat="server" 
                            style="z-index: 1; position: absolute; top: 937px; left: 554px; width: 160px; height: 29px; right: 320px;" 
                            Text="Save &amp; Next" />
                    
                    </p>
                </td>
            </tr>
        </table>
        
        <br />
    </p>
</asp:Content>

