﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="Activities.aspx.vb" Inherits="Activities" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="background-color: #FFFFFF; font-family: 'comic Sans MS'; font-size: medium; font-weight: 500;">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label1" runat="server" Font-Size="Medium" ForeColor="Fuchsia" 
        Text=" WE SHAPE THOSE WHO SHAPE THE WORLD "></asp:Label>
    <br />
    <br />
    <asp:Image ID="Image1" runat="server" Height="281px" 
        ImageUrl="~/PLACEMENT IMAGES/placement_act.png" />
    <br />
    <br />
    OPUS, like many other placement management web sites, provides information on 
    placement pro-viders and the placements they offer so that students may view and 
    assess their opportunities. Indeed, all higher education institutions (HEIs) 
    have well-developed web sites to inform their students of va-cancies and how to 
    prepare for their work-integrated learning experience. However, relatively few 
    have developed the means to enable the student, the provider and the placements 
    coordinator (with others) to take effective actions on the web as a follow-on 
    from the information they have viewed. It is too often the case that information 
    must be acted upon by wholly separate means away from the web. Changes and 
    updates are laborious for staff and are at risk of being late. With OPUS the 
    user may control their information, the provider may promote their 
    opportunities, the student can take action to apply for any<br />
    vacancy and the placements coordinator may control all features. Being on the 
    web, all information may be accessed from anywhere at anytime. This paper sets 
    out the principles used to guide the development of OPUS and how it delivers 
    effec-tive placement management for the University of Ulster. As a closing 
    comment some views of diverse users are used to demonstrate its effectiveness.<br />
</p>
</asp:Content>

