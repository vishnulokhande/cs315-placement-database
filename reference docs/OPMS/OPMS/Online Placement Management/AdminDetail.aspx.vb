﻿Imports System.Data.OleDb
Imports System.Data

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub DropDownList6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList6.SelectedIndexChanged


        Dim s As String
        s = DropDownList6.Text
        Dim selectString As String = "SELECT LoginInfo.UserName, ResumeDetailInfo.JobPreference, ResumeDetailInfo.ResumeTitle, ProfessionalInfo.ResumeCategory, ProfessionalInfo.KeySkills, ProfessionalInfo.YourSelfAndPreferredJob, ProfessionalInfo.TotalWorkExp, ProfessionalInfo.CurrentSalary, ProfessionalInfo.EducationDetails, PersonalInfo.YourName, PersonalInfo.Gender, PersonalInfo.DateOfBirth, PersonalInfo.Country, PersonalInfo.State, PersonalInfo.City, PersonalInfo.StreetAddress, PersonalInfo.MobileNumber, PersonalInfo.Emailid FROM ((LoginInfo INNER JOIN PersonalInfo ON LoginInfo.UserName = PersonalInfo.UserName) INNER JOIN ProfessionalInfo ON LoginInfo.UserName = ProfessionalInfo.UserName) INNER JOIN ResumeDetailInfo ON LoginInfo.UserName = ResumeDetailInfo.UserName where ProfessionalInfo.ResumeCategory= '" & s & "' "
        Dim ds As DataSet = GetData(selectString)
        If (ds.Tables.Count > 0) Then

            GridView1.DataSource = ds
            GridView1.DataBind()


        Else

            'Message.Text = "Unable to connect to the database."

        End If



        'Dim row As GridViewRow = GridView1.SelectedRow

        'TextBox1.Text = "" & row.Cells(18).Text

        ''Dim mail As New System.Net.Mail.MailMessage("you@yourcompany.com", TextBox1.Text)
        ''Dim SmtpMail As New SmtpClient("localhost")
        ''mail.To = "sdsd"
        ''mail.To = TextBox1.Text
        ''mail.From = "you@yourcompany.com"
        ''mail.Subject = "this is a test email."
        ''mail.Body = "this is my test email body"
        ''SmtpMail.SmtpServer = "localhost" 'your real server goes here
        ''SmtpMail.Send(mail)


    End Sub

    Function GetData(ByVal queryString As String) As DataSet

        ' Retrieve the connection string stored in the Web.config file.
        'Dim connectionString As String = ConfigurationManager.ConnectionStrings("NorthWindConnectionString").ConnectionString
        Dim connectString As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"
        Dim cn As OleDbConnection = New OleDbConnection(connectString)
        cn.Open()

        Dim ds As New DataSet()

        Try

            ' Connect to the database and run the query.
            'Dim connection As New SqlConnection(connectString)
            Dim adapter As New OleDbDataAdapter(queryString, cn)

            ' Fill the DataSet.
            adapter.Fill(ds)


        Catch ex As Exception

            ' The connection failed. Display an error message.
            ' Message.Text = "Unable to connect to the database."

        End Try

        Return ds

    End Function


    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Redirect("Adminrpt.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = "ADMIN DETAIL"
    End Sub
End Class
