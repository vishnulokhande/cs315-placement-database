﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="Services.aspx.vb" Inherits="Services" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="background-color: #FFFFFF; font-family: 'comic Sans MS'; font-size: medium; font-weight: 500;">
        <asp:Image ID="Image1" runat="server" Height="392px" 
            ImageUrl="~/PLACEMENT IMAGES/SolutionsSlide.jpg" />
    <br />
        i. Data Security – to be of value to users the system must hold personal data 
        for the student and other personnel. The students’ CV data, staff and company 
        contact details and assess-ment results are considered to be personal data that 
        should not be accessible by the wider world (unless specified in the course of 
        defined processes). Also, the details of placement providers and opportunities 
        may be considered as ‘commercial in confidence’ information that should not be 
        available to any competitor or student not entitled to receive them. Principles 
        for a Web-based Program ii. Privacy – among authorised users the personal data 
        of one should not be viewable by anoth-er, unless the owner had chosen 
        otherwise. However, the placements coordinator or admin-istrator (or any person 
        who had similar privileges assigned) needs viewing and editing rights over 
        almost all data. iii. Intuitive – to render unnecessary a significant training 
        programme for new users any fea-tures of the system must be as near-intuitive as 
        possible and follow current conventions ap-plicable to current web usage. 
        Alternatively, brief guidance needs to be available at the point of use. iv. 
        Reliable – staff and others have established a level of confidence in their 
        existing systems for work-integrated learning management. They have ready access 
        to correcting faults in manual paper-based systems and, despite complexity and 
        inefficiency, these systems can be made to function. Any migration to a 
        web-based system must achieve continuous, reliable service. v. One Data Source – 
        one of the difficulties with non-electronic systems is that if data is held in 
        more than one location updating become tedious and highly error prone. This 
        web-based system holds data in one location only so that one updating enables 
        all views of it to be cor-rect and current. Frequent back-up of data must follow 
        current good practice to provide for technical failures or errors.<br />
        Students need information and support in essentially three phases: pre-, in- and 
        post-placement. In all circumstances each user must be listed as authorised to 
        use OPUS with a unique username and password (which can be refreshed easily). On 
        every login they see the an-nouncements page (which is editable by the 
        placements coordinator) and changes in companies and vacancies since their last 
        login. Any student may see only their own personal data and not that of any 
        other person. OPUS has a facility to deny any student the right to use the 
        system if the placements coordinator wishes to apply this status. A targeted 
        help list is provided for the student throughout their use of OPUS as the system 
        recognises their login and selects the appro-priate staff from the list of 
        administrators.<br />
        <br />
        <asp:Image ID="Image2" runat="server" Height="329px" 
            ImageUrl="~/PLACEMENT IMAGES/fw_statistics.png" />
        <br />
</p>
</asp:Content>

