﻿Imports System.Data.OleDb
Imports System.Data
Partial Class AdminLogin
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click


        Dim connString As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"
        Dim qryString As String = "SELECT * FROM AdminLogin;"
        Dim objConn As New OleDbConnection(connString)
        Dim objCmd As New OleDbCommand(qryString, objConn)
        Dim myReader As OleDbDataReader
        Dim auth As String = Nothing

        Try
            Dim gotUser As Boolean = False
            objConn.Open()
            myReader = objCmd.ExecuteReader

            While myReader.Read
                If myReader.Item("UserName") = TextBox1.Text Then
                    gotUser = True
                    auth = CheckPwd(myReader.Item("UserName"), myReader.Item("Password1"))
                End If

            End While

            myReader.Close()

            If gotUser = False Then

                Label1.Text = "You are not authorized for access"

            Else

                Select Case auth
                    Case True
                        'FormsAuthentication.RedirectFromLoginPage(TextBox1.Text, False)
                        Response.Redirect("AdminDetail.aspx")
                    Case Else

                        Label1.Text = "Incorrect Password"

                End Select

            End If

        Catch ex As Exception

            'MsgBox(ex.ToString)

        Finally
            objConn.Close()
        End Try



    End Sub
    Function CheckPwd(ByVal username As String, ByVal pwd As String) As Boolean

        Dim Authorized As Boolean = False
        Dim pwdFromUser As String = TextBox2.Text

        If pwdFromUser = pwd Then

            Authorized = True

        Else
            Authorized = False
        End If

        Return Authorized

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Title = "ADMIN LOGIN"

        Label2.Visible = False
        TextBox3.Visible = False
        Label3.Visible = False
        TextBox4.Visible = False
        Button2.Visible = False





    End Sub

   

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        Label2.Visible = True
        TextBox3.Visible = True
        Label3.Visible = True
        TextBox4.Visible = True
        Button2.Visible = True

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        If TextBox3.Text = "" Then
            MsgBox("Old Password Cannot Be Left Blank")

        ElseIf TextBox4.Text = "" Then
            MsgBox("New Password Cannot Be Left Blank")


        Else

            Dim connString As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"
            Dim qryString As String = "SELECT * FROM AdminLogin;"
            Dim objConn As New OleDbConnection(connString)
            Dim objCmd As New OleDbCommand(qryString, objConn)
            Dim myReader As OleDbDataReader
            Dim auth As String = Nothing
            Dim gotUser As Boolean = True



            objConn.Open()
            myReader = objCmd.ExecuteReader

            While myReader.Read

                If myReader.Item("Password1") <> TextBox3.Text Then
                    gotUser = False

                End If



            End While


            If gotUser = False Then

                MsgBox("Incorrect Password")

            Else



                Dim connectString1 As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"
                Dim cn1 As OleDbConnection = New OleDbConnection(connectString1)
                Dim selectString1 As String = "UPDATE AdminLogin set Password1='" & TextBox4.Text & "'"

                Dim cmd1 As OleDbCommand = New OleDbCommand(selectString1, cn1)
                cn1.Open()
                cmd1.ExecuteNonQuery()
                MsgBox("Your Password Updated")


            End If
        End If
    End Sub
End Class
