﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="UpdateProfessionalInfo.aspx.vb" Inherits="UpdateProfessionalInfo" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="background-position: center center; background-image: url('backgroundimages/background.jpg'); background-repeat: no-repeat; background-attachment: fixed; table-layout: auto;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" 
            width="100%" 
            style="background-image: url('backgroundimages/background.jpg');">
            <tr valign="top">
                <td class="p10px registration_h b large ttu" colspan="2" 
                    style="font-family: 'comic Sans MS'; font-size: x-large; font-weight: 500; font-style: normal; color: #FFFFFF; background-color: #000000;">
                    <marquee behavior="">Update Professional Information</marquee><br />
                    <br />
                                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar">
                    <span class="red">* Select Resume Category</span></td>
                <td class="p5px10px">
                    <asp:DropDownList ID="DropDownList6" runat="server" Height="22px" Width="202px">
                        <asp:ListItem>Accounting / Tax</asp:ListItem>
                        <asp:ListItem>Banking</asp:ListItem>
                        <asp:ListItem>Biotech / Scientist</asp:ListItem>
                        <asp:ListItem>CallCentre / Operations</asp:ListItem>
                        <asp:ListItem>Computers / IT</asp:ListItem>
                        <asp:ListItem>Construction / Realestate</asp:ListItem>
                        <asp:ListItem>Editors / Journalism</asp:ListItem>
                        <asp:ListItem>Engineering</asp:ListItem>
                        <asp:ListItem>Export / Import</asp:ListItem>
                        <asp:ListItem>Financial Services</asp:ListItem>
                        <asp:ListItem>Fresh Graduate</asp:ListItem>
                        <asp:ListItem>Hospitality Travel</asp:ListItem>
                        <asp:ListItem>HR / Top Management</asp:ListItem>
                        <asp:ListItem>Insurance</asp:ListItem>
                        <asp:ListItem>Legal Law</asp:ListItem>
                        <asp:ListItem>Manufacturing / Production</asp:ListItem>
                        <asp:ListItem>Media / Entertainment</asp:ListItem>
                        <asp:ListItem>Medical</asp:ListItem>
                        <asp:ListItem>Oil / Gas / Petrolem</asp:ListItem>
                        <asp:ListItem>Public Relation</asp:ListItem>
                        <asp:ListItem>Sales / Marketing</asp:ListItem>
                        <asp:ListItem>Security</asp:ListItem>
                        <asp:ListItem>Teaching / Education</asp:ListItem>
                        <asp:ListItem>Technical</asp:ListItem>
                        <asp:ListItem>Telecom</asp:ListItem>
                        <asp:ListItem>Trade</asp:ListItem>
                        <asp:ListItem>Transportation / Logistic</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <span class="small" style="color: #FFFFFF; font-weight: 500;">(Select the 
                    Category In Which you want to Post Your RESUME)<br />
                    </span></td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar">
                    <span class="red">* </span>Key Skills
                </td>
                <td class="p5px10px">
                    <asp:TextBox ID="TextBox13" runat="server" Width="365px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBox13"
                    
                        ErrorMessage=" * KeySkills Cannot Be Left Blank"></asp:RequiredFieldValidator>
                    <br />
                    <span class="small uu" style="color: #FFFFFF; font-weight: 500">(Enter You Key 
                    Skills Separated By Comma e.g Marketing, PHP, JSP, ASP, CA, Scientist etc.)<br />
                    </span></td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar">
                    <span class="red">*</span> About Yourself &amp; Preferred Job
                    <br />
                    (Upto 500 characters)</td>
                <td class="p5px10px">
                    <asp:TextBox ID="TextBox14" runat="server" Height="115px" TextMode="MultiLine" 
                        Width="412px" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TextBox14"
                        ErrorMessage=" * Cannot Be Left Blank"></asp:RequiredFieldValidator>
                    <br />
                    &nbsp;</td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar">
                    <span class="red">* Total Work Experience</span></td>
                <td class="p5px10px">
                    <asp:DropDownList ID="year" runat="server">
                        <asp:ListItem value="0 Years">0 Years</asp:ListItem>
                        <asp:ListItem value="1 Years">1 Years</asp:ListItem>
                        <asp:ListItem value="2 Years">2 Years</asp:ListItem>
                        <asp:ListItem value="3 Years">3 Years</asp:ListItem>
                        <asp:ListItem value="4 Years">4 Years</asp:ListItem>
                        <asp:ListItem value="5 Years">5 Years</asp:ListItem>
                        <asp:ListItem value="6 Years">6 Years</asp:ListItem>
                        <asp:ListItem value="7 Years">7 Years</asp:ListItem>
                        <asp:ListItem value="8 Years">8 Years</asp:ListItem>
                        <asp:ListItem value="9 Years">9 Years</asp:ListItem>
                        <asp:ListItem value="10 Years">10 Years</asp:ListItem>
                        <asp:ListItem value="11 Years">11 Years</asp:ListItem>
                        <asp:ListItem value="12 Years">12 Years</asp:ListItem>
                        <asp:ListItem value="13 Years">13 Years</asp:ListItem>
                        <asp:ListItem value="14 Years">14 Years</asp:ListItem>
                        <asp:ListItem value="15 Years">15 Years</asp:ListItem>
                        <asp:ListItem value="16 Years">16 Years</asp:ListItem>
                        <asp:ListItem value="17 Years">17 Years</asp:ListItem>
                        <asp:ListItem value="18 Years">18 Years</asp:ListItem>
                        <asp:ListItem value="19 Years">19 Years</asp:ListItem>
                    </asp:DropDownList> &nbsp;
                    <asp:DropDownList id="total_work_exp_month" runat="server">
                        <asp:ListItem value="1 Months">1 Months</asp:ListItem>
                        <asp:ListItem value="2 Months">2 Months</asp:ListItem>
                        <asp:ListItem value="3 Months">3 Months</asp:ListItem>
                        <asp:ListItem value="4 Months">4 Months</asp:ListItem>
                        <asp:ListItem value="5 Months">5 Months</asp:ListItem>
                        <asp:ListItem value="6 Months">6 Months</asp:ListItem>
                        <asp:ListItem value="7 Months">7 Months</asp:ListItem>
                        <asp:ListItem value="8 Months">8 Months</asp:ListItem>
                        <asp:ListItem value="9 Months">9 Months</asp:ListItem>
                        <asp:ListItem value="10 Months">10 Months</asp:ListItem>
                        <asp:ListItem value="11 Months">11 Months</asp:ListItem>
                        <asp:ListItem value="12 Months">12 Months</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    
                    </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar">
                    *<span class="red"> Current Salary (Per Month Basis)</span></td>
                <td class="p5px10px">
                    <asp:DropDownList ID="salary" runat="server" Height="23px" Width="302px">
                        <asp:ListItem value="Negotiable">Negotiable</asp:ListItem>
                        <asp:ListItem value="Up To 5,000">Up To 5,000</asp:ListItem>
                        <asp:ListItem value="5,000-10,000">5,000-10,000</asp:ListItem>
                        <asp:ListItem value="10,000-15,000">10,000-15,000</asp:ListItem>
                        <asp:ListItem value="15,000-20,000">15,000-20,000</asp:ListItem>
                        <asp:ListItem value="20,000-25,000">20,000-25,000</asp:ListItem>
                        <asp:ListItem value="25,000-30,000">25,000-30,000</asp:ListItem>
                        <asp:ListItem value="30,000-35,000">30,000-35,000</asp:ListItem>
                        <asp:ListItem value="35,000-40,000">35,000-40,000</asp:ListItem>
                        <asp:ListItem value="40,000-45,000">40,000-45,000</asp:ListItem>
                        <asp:ListItem value="45,000-50,000">45,000-50,000</asp:ListItem>
                        <asp:ListItem value="50,000-55,000">50,000-55,000</asp:ListItem>
                        <asp:ListItem value="55,000-60,000">55,000-60,000</asp:ListItem>
                        <asp:ListItem value="60,000-65,000">60,000-65,000</asp:ListItem>
                        <asp:ListItem value="65,000-75,000">65,000-75,000</asp:ListItem>
                        <asp:ListItem value="75,000-80,000">75,000-80,000</asp:ListItem>
                        <asp:ListItem value="80,000-85,000">80,000-85,000</asp:ListItem>
                        <asp:ListItem value="85,000-90,000">85,000-90,000</asp:ListItem>
                        <asp:ListItem value="90,000+">90,000+</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar">
                    <span class="red">* Education Details</span></td>
                <td class="p5px10px">
                
                    <asp:ListBox ID="edudetails" runat="server" SelectionMode="Multiple" 
                        
                        
                        
                        
                        
                        style="z-index: 1; position: absolute; top: 581px; left: 464px; width: 131px; height: 58px;">    <asp:ListItem>Higher 
                        Secondary</asp:ListItem>
                        <asp:ListItem>Secondary School</asp:ListItem>
                        <asp:ListItem>Vocational Course</asp:ListItem>
                        <asp:ListItem>Diploma</asp:ListItem>
                        <asp:ListItem>Advanced/Higher Diploma</asp:ListItem>
                        <asp:ListItem>Professional Degree</asp:ListItem>
                        <asp:ListItem>Bachelor Degree</asp:ListItem>
                        <asp:ListItem>B.A</asp:ListItem>
                        <asp:ListItem>B.Arch</asp:ListItem>
                        <asp:ListItem>B.C.A</asp:ListItem>
                        <asp:ListItem>B.B.A</asp:ListItem>
                        <asp:ListItem>B.Com</asp:ListItem>
                        <asp:ListItem>B.Ed</asp:ListItem>
                        <asp:ListItem>BDS</asp:ListItem>
                        <asp:ListItem>BHM</asp:ListItem>
                        <asp:ListItem>B.Pharma</asp:ListItem>
                        <asp:ListItem>B.Sc</asp:ListItem>
                        <asp:ListItem>B.Tech/B.E</asp:ListItem>
                        <asp:ListItem>LLB</asp:ListItem>
                        <asp:ListItem>MBBS</asp:ListItem>
                        <asp:ListItem>BVSC</asp:ListItem>
                        <asp:ListItem>Master Degree</asp:ListItem>
                        <asp:ListItem>M.A</asp:ListItem>
                        <asp:ListItem>M.Arch</asp:ListItem>
                        <asp:ListItem>M.C.A</asp:ListItem>
                        <asp:ListItem>M.B.A/PGDM</asp:ListItem>
                        <asp:ListItem>M.Com</asp:ListItem>
                        <asp:ListItem>M.Ed</asp:ListItem>
                        <asp:ListItem>MS</asp:ListItem>
                        <asp:ListItem>M.Pharma</asp:ListItem>
                        <asp:ListItem>M.Sc</asp:ListItem>
                        <asp:ListItem>M.Tech</asp:ListItem>
                        <asp:ListItem>LLM</asp:ListItem>
                        <asp:ListItem>MVSC</asp:ListItem>
                        <asp:ListItem>CA</asp:ListItem>
                        <asp:ListItem>CS</asp:ListItem>
                        <asp:ListItem>ICWA</asp:ListItem>
                        <asp:ListItem>Integrated PG</asp:ListItem>
                        <asp:ListItem>Post Graduate Diploma</asp:ListItem>
                        <asp:ListItem>Doctorate Degree</asp:ListItem>
                        <asp:ListItem>Some Tertiary Coursework</asp:ListItem>
                        <asp:ListItem>Ph.D/Doctorate</asp:ListItem>
                        <asp:ListItem>MPHIL</asp:ListItem>
                        <asp:ListItem>Others</asp:ListItem>
                    </asp:ListBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a ]</div="" class="red u" href="javascript:void(0);" 
                                onclick="javascript:removeAllEducation();" t=""><asp:TextBox 
                        ID="TextBox15" runat="server" 
                                        style="z-index: 1; position: absolute; top: 578px; left: 611px; height: 34px; width: 246px;" 
                                        TextMode="MultiLine"></asp:TextBox>
                        </a>
                    <br />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                    <br />
                    <div id="educationdetail_container">
                        <div>
                            <a ]</div="" class="red u" href="javascript:void(0);" 
                                onclick="javascript:removeAllEducation();" t="">
                            <div id="educationParent">
                                <div id="educationdetail_display">
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </td>
            </tr>
            <tr valign="top">
                <td class="p10px" colspan="2">
                    <p class="bdrBlight">
                        <img src="PLACEMENT%20IMAGES/placements.jpg" /><asp:Button ID="Button2" 
                            runat="server" 
                            style="z-index: 1; position: absolute; top: 621px; left: 640px; width: 202px; height: 24px;" 
                            Text="Click To Add Education Details" />
                    </p>
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:Button ID="Button1" runat="server" 
            style="z-index: 1; position: absolute; top: 761px; left: 510px; height: 27px; width: 170px" 
            Text="Update" />
        <br />
        <br />
        <br />
    </p>
</asp:Content>

