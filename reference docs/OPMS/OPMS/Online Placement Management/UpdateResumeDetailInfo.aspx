﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="UpdateResumeDetailInfo.aspx.vb" Inherits="UpdateResumeDetailInfo" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="background-image: url('backgroundimages/background.jpg'); background-repeat: no-repeat; background-attachment: fixed; background-position: center center; font-family: 'comic Sans MS'; font-size: large; font-weight: 500; font-style: normal; font-variant: normal; color: #000000;">
        
        <table align="center" border="0" cellpadding="0" cellspacing="0" class="bdr" 
            width="100%" 
            
            style="background-image: url('backgroundimages/background.jpg'); background-repeat: no-repeat; background-attachment: fixed; background-position: center center; margin-top: 0; font-family: 'comic Sans MS'; font-size: large; font-weight: 500; font-style: normal; font-variant: normal; color: #000000;">
            <tr valign="top">
                <td class="p10px registration_h b large ttu" colspan="2" 
                    
                    
                    style="background-position: center center; font-family: 'comic Sans MS'; font-size: large; font-weight: 500; font-style: normal; color: #FFFFFF; height: 69px; background-color: #000000;">
                    &nbsp;<br />
                    <marquee behavior="">Update Resume Details Information</marquee><br />
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" 
                    style="height: 50px; font-family: 'comic Sans MS'; font-size: large; font-weight: 500; font-style: normal; color: #000000;">
                    &nbsp; *Job Preference</td>
                <td class="p5px10px" style="height: 50px">
                    <asp:RadioButton ID="rfulltime" runat="server" Text="   Full Time" 
                        GroupName="r1" />
                    &nbsp;<asp:RadioButton ID="rparttime" runat="server" Text="   Part Time" 
                        GroupName="r1" />
                    &nbsp;<asp:RadioButton ID="rfreelancer" runat="server" Text="   Freelancer" 
                        GroupName="r1" />
                    <br />
                </td>
            </tr>
            <tr valign="top">
                <td class="p5px10px ar" style="height: 141px">
                    <span class="red">&nbsp;* </span>Resume Title<br />
                    (Maximum 150 Characters)</td>
                <td class="p5px10px" style="height: 141px">
                    <asp:TextBox ID="txtrestit" runat="server" Width="351px"></asp:TextBox>
                    &nbsp; Length :
                    <asp:TextBox ID="TextBox16" runat="server" Width="38px" Height="20px" 
                        MaxLength="100"></asp:TextBox>
                    <br />
                    <span class="small">
                    &nbsp;Example<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtrestit"
                        ErrorMessage=" * Resume Title Cannot Be Left Blank" Font-Size="Small"></asp:RequiredFieldValidator>
                    <br />
                    - BE,MBA 17yrs exp. in Customer Support/Project management/Operations<br />
                    - MCA,MTech (java,.net,PHP,telecommunication) have 7yrs exp. in Customer 
                    Support/Project management/Operations<br />
                    - B.Tech in Computer Science (Fresher)<br />
                    <br />
                    </span></td>
            </tr>
            <tr valign="top">
                <span class="small">
                <td class="p5px10px ar" style="height: 109px">
                    &nbsp; * Select Your Resume To Upload</td>
                <td class="p5px10px" style="height: 109px">
                    <br />
                    <asp:FileUpload ID="fileUpEx" runat="server" Width="229px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button2" runat="server" Text="Upload" Width="143px" />
                    <br />
                    <br />
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </td>
                </span>
            </tr>
            <tr valign="top">
                <span class="small">
                <td class="p5px10px ar">
                    &nbsp;</td>
                <td class="p5px10px">
                    &nbsp;</td>
                &nbsp;</td>
                </span>
            </tr>
            <tr valign="top">
                <span class="small">
                <td class="p10px" colspan="2">
                    <p class="bdrBlight">
                        <img src="PLACEMENT%20IMAGES/tpe.jpg" /></p>
                </td>
                </span>
            </tr>
            <tr valign="top">
                <span class="small">
                <td>
                    &nbsp;</td>
                <td class="p10px">
                    <p class="bdrBlight">
                        <a class="u uu" href="javascript:" 
                            onclick="window.open('/terms-seeker.htm','NewWin','toolbar=no,menubar=no,height=500,width=700,scrollbars=yes');">
                        &nbsp;</td>
                </span>
            </tr>
            <tr valign="top">
                <span class="small">
                <td>
                    &nbsp;</td>
                <td class="p5px20px">
                    <div class="prb">
                        <asp:Button ID="Button1" runat="server" Text="Save &amp; Next" />
                    </div>
                </td>
                </span>
            </tr>
            <tr valign="top">
                <span class="small">
                <td colspan="2" height="40">
                    &nbsp;</td>
                </span>
            </tr>
        </table>
    </p>
    <p style="background-color: #FFFFFF">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </p>
</asp:Content>

