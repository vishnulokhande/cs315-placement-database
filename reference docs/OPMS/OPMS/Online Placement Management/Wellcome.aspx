﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="Wellcome.aspx.vb" Inherits="Wellcome" title="Untitled Page" %>



<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">

        
        
	<div id="main" 
        
    
    
        style="background-color: #FFFFFF; width: 479px; background-image: none; visibility: visible; display: block; height: 1458px;">
		<div id="welcome">
			<h2 style="height: 254px; background-image: url('PLACEMENT%20IMAGES/placements_banner.jpg'); width: 476px; margin-top: 0; font-family: 'comic Sans MS'; font-size: large; font-weight: 500; color: #000000; text-decoration: blink;">
                <marquee behavior="">Welcome To Online Placement Management !</marquee><br />
                
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </h2>
		</div>
		<div id="example" style="height: 1124px; background-color: #C0C0C0;">
			<h2 style="font-family: 'comic Sans MS'; font-size: large; font-weight: 500; color: #000000;">
                I. INTRODUCTION<br />
            </h2>
			<p style="font-family: 'comic Sans MS'; font-size: small; font-weight: 500; color: #FFFFFF; background-color: #000000;">
                OPUS, like many other placement management web sites, provides information on 
                placement pro-viders and the placements they offer so that students may view and 
                assess their opportunities. Indeed, all higher education institutions (HEIs) 
                have well-developed web sites to inform their students of va-cancies and how to 
                prepare for their work-integrated learning experience. However, relatively few 
                have developed the means to enable the student, the provider and the placements 
                coordinator (with others) to take effective actions on the web as a follow-on 
                from the information they have viewed. It is too often the case that information 
                must be acted upon by wholly separate means away from the web. Changes and 
                updates are laborious for staff and are at risk of being late. With OPUS the 
                user may control their information, the provider may promote their 
                opportunities, the student can take action to apply for any vacancy and the 
                placements coordinator may control all features. Being on the web, all 
                information may be accessed from anywhere at anytime.</p>
			   <h3 style="font-family: 'comic Sans MS'; font-size: large; font-weight: 500; color: #000000"> 
                   II. PRINCIPLES SUPPORTED BY OPMS</h3>
			<p style="font-family: 'comic Sans MS'; font-size: small; font-weight: 500; background-color: #000000; color: #FFFFFF; height: 192px;">
                From the earliest development we realized that, to be effective in replacing 
                existing complex sys-tems, OPUS needed to support several principles associated 
                with a web-based application and the con-duct of work-integrated learning in 
                compliance with the United Kingdom Quality Assurance Agency Code of Practice, 
                Section 9 (the Code) (2007). Further, several other principles were necessary to 
                commend OPUS to work-based learning practitioners and IT support personnel for 
                their future opera-tions. These principles are developed below..<br />
                <br />
            </p>
            <p style="font-family: 'comic Sans MS'; font-size: small; font-weight: 500; background-color: #000000; color: #FFFFFF; height: 472px; background-image: url('PLACEMENT IMAGES/100placement.gif'); background-repeat: no-repeat; background-attachment: scroll;">
                &nbsp;</p>
				
		    <br />
            <br />
            <br />
            <br />
            <br />
				
		</div>
	</div>
	<div id="sidebar" style="background-color: #FFFFFF; height: 1457px;">
		<div id="login" class="boxed" style="background-color: #FFFFFF">
			<h2 class="title" style="margin-top: 0; background-color: #000000;">Client Account</h2>
			<div class="content" style="height: 194px; background-color: #C0C0C0;">
				<form id="form2" method="post" action="#">
					<fieldset>
					<legend>Sign-In</legend>
					<label for="inputtext1">Client ID:</label>
					<asp:TextBox ID="TextBox1" runat="server" Width="178px" Height="22px"></asp:TextBox>
					<label for="inputtext2">Password:</label>
					<asp:TextBox ID="TextBox2" 
                    runat="server" Width="177px" Height="22px" TextMode="Password"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button 
       ID="Button1" runat="server" Text="Submit" Width="179px" BackColor="Black" ForeColor="White" />&nbsp;&nbsp;<p><a href="ForgetPassword.aspx">
                            Forgot your password?</a></p>
					<p><a href="LoginInfo.aspx">Register Now</a><br />
                        <br />
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                                    </p>
					</fieldset>
				</form>
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title" style="background-color: #000000"><marquee behavior="">Recent Updates</marquee><br /></h2>
			<div class="content" style="background-color: #C0C0C0">
			    `</div>
		</div>
		<div id="partners" class="boxed">
			<h2 class="title" style="background-color: #000000"><marquee behavior="">Our Partners</marquee></h2>
			<div class="content" 
                style="background-color: #C0C0C0; height: 842px; margin-top: 7;">
			</div>
		</div>
	</div>
	

</asp:Content>
