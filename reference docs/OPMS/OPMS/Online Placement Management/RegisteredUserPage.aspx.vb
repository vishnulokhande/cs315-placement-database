﻿Imports System.Data.OleDb
Imports System.Data

Partial Class RegisteredUserPage

    Inherits System.Web.UI.Page

    Dim Connection As OleDbConnection
    Dim Command As OleDbCommand
    Dim DataReader As OleDbDataReader
    Dim uname As String
    Dim value As String
    Dim r1 As String
    Dim r2 As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Title = "WELLCOME"

        Try

            uname = Session("uname")
            Label1.Text = "WelCome" & " " & " " & uname

            Dim connectString As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"
            Dim cn As OleDbConnection = New OleDbConnection(connectString)
            cn.Open()
            Dim selectString As String = "SELECT LoginInfo.UserName, LoginInfo.Password1,  PersonalInfo.YourName, PersonalInfo.Gender, PersonalInfo.DateOfBirth, PersonalInfo.Country, PersonalInfo.State, PersonalInfo.City, PersonalInfo.StreetAddress, PersonalInfo.MobileNumber, PersonalInfo.Emailid, ProfessionalInfo.ResumeCategory, ProfessionalInfo.KeySkills, ProfessionalInfo.YourSelfAndPreferredJob, ProfessionalInfo.TotalWorkExp, ProfessionalInfo.CurrentSalary, ProfessionalInfo.EducationDetails, ResumeDetailInfo.JobPreference, ResumeDetailInfo.ResumeTitle FROM ((LoginInfo INNER JOIN PersonalInfo ON LoginInfo.UserName = PersonalInfo.UserName) INNER JOIN ProfessionalInfo ON LoginInfo.UserName = ProfessionalInfo.UserName) INNER JOIN ResumeDetailInfo ON LoginInfo.UserName = ResumeDetailInfo.UserName where LoginInfo.Username= '" & uname & "'"


            Dim cmdlf As OleDbCommand = New OleDbCommand(selectString, cn)
            Dim readerlf As OleDbDataReader = cmdlf.ExecuteReader()
            While readerlf.Read
                txtuname.Text = readerlf("UserName")
                txtpass.Text = readerlf("Password1")
            End While



            Dim cmd1 As OleDbCommand = New OleDbCommand(selectString, cn)
            Dim reader1 As OleDbDataReader = cmd1.ExecuteReader()
            While reader1.Read
                r1 = reader1("Gender")
            End While


            Dim cmdpi As OleDbCommand = New OleDbCommand(selectString, cn)
            Dim readerpi As OleDbDataReader = cmdpi.ExecuteReader()
            While readerpi.Read

                txtyname.Text = readerpi("YourName")

                If r1 = "Male" Then
                    rmale.Checked = True
                ElseIf r1 = "Female" Then
                    rfemale.Checked = True
                End If



                txtdate.Text = readerpi("DateOfBirth")
                COUNTRY.Text = readerpi("Country")
                txtstate.Text = readerpi("State")
                txtcity.Text = readerpi("City")
                txtadd.Text = readerpi("StreetAddress")
                txtmobno.Text = readerpi("MobileNumber")
                txtemail.Text = readerpi("Emailid")


            End While




            Dim cmdpr As OleDbCommand = New OleDbCommand(selectString, cn)
            Dim readerpr As OleDbDataReader = cmdpr.ExecuteReader()

            While readerpr.Read

                DropDownList6.Text = readerpr("ResumeCategory")
                txtkeyskill.Text = readerpr("KeySkills")
                txtyself.Text = readerpr("YourSelfAndPreferredJob")
                txtworkexp.Text = readerpr("TotalWorkExp")
                txtedudetails.Text = readerpr("EducationDetails")
                salary.Text = readerpr("CurrentSalary")
                'edudetails.Text = readerpr("EducationDetails")


            End While


            Dim cmdrdi As OleDbCommand = New OleDbCommand(selectString, cn)
            Dim readerrdi As OleDbDataReader = cmdrdi.ExecuteReader()
            While readerrdi.Read
                r2 = readerrdi("JobPreference")
            End While

            Dim cmdrd As OleDbCommand = New OleDbCommand(selectString, cn)
            Dim readerrd As OleDbDataReader = cmdrd.ExecuteReader()

            While readerrd.Read

                If r2 = "fulltime" Then
                    rfulltime.Checked = True

                ElseIf r2 = "parttime" Then
                    rparttime.Checked = True
                ElseIf r2 = "freelancer" Then
                    rfreelancer.Checked = True

                End If

                txtrestit.Text = readerrd("ResumeTitle")

            End While

            cn.Close()

            'txtuname.Text = uname
        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try

    End Sub

    


    Protected Sub TextBox17_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtedudetails.TextChanged






    End Sub


    Protected Sub txtuname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtuname.TextChanged

    End Sub

    Protected Sub rmale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rmale.CheckedChanged

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Session.Remove(uname)
        Session.Clear()
        If Session.IsNewSession Then

            Session.Remove(uname)

        End If

        Response.Redirect("Wellcome.aspx")

    End Sub


    Protected Sub Button1_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Response.Redirect("UpdateLoginInfo.aspx")

        'Dim connectString1 As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"

        'Dim cn1 As OleDbConnection = New OleDbConnection(connectString1)
        'Dim selectString1 As String = "UPDATE LoginInfo set Password1='" & txtpass.Text & "' where UserName ='" & uname & "'"


        ''Dim selectString1 As String = "UPDATE LoginInfo set Password1='" & txtpass.Text & "' where UserName ='" & uname & "'"

        'Dim cmd1 As OleDbCommand = New OleDbCommand(selectString1, cn1)
        'cn1.Open()
        'MsgBox(cn1.ToString)
        'cmd1.ExecuteNonQuery()
        'MsgBox("updated")
        'cn1.Close()






    End Sub

    Protected Sub txtpass_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtpass.TextChanged

    End Sub

   

    

  

    Protected Sub txtmobno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtmobno.TextChanged

    End Sub

    

   
    
    
  
   
    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        Response.Redirect("UpdatePersonalInfo.aspx")
        ''If rm.Checked = True Then
        ''    value = "Male"
        ''ElseIf rf.Checked = True Then
        ''    value = "Female"

        ''End If




        'Dim connectString1 As String = "Provider=Microsoft.Jet.OleDb.4.0;Data Source=" & Server.MapPath("OPMS.mdb") & ";"

        'Dim cn1 As OleDbConnection = New OleDbConnection(connectString1)



        'Dim selectString1 As String = "UPDATE PersonalInfo set YourName='" & txtyname.Text & "',Gender='" & value & "',DateOfBirth='" & txtdate.Text & "',Country='" & COUNTRY.Text & "',State='" & txtstate.Text & "',City='" & txtcity.Text & "',StreetAddress='" & txtadd.Text & "',MobileNumber='" & txtmobno.Text & "',Emailid='" & txtemail.Text & "'where UserName ='" & uname & "'"

        'Dim cmd1 As OleDbCommand = New OleDbCommand(selectString1, cn1)
        'cn1.Open()
        'cmd1.ExecuteNonQuery()
        'Button1.Enabled = False
        'MsgBox("Your Personal Information Updated")
        ''Response.Redirect("RegisteredUserPage.aspx")
        'cn1.Close()




    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click

        Response.Redirect("UpdateProfessionalInfo.aspx")

    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
        Response.Redirect("UpdateResumeDetailInfo.aspx")
    End Sub
End Class
