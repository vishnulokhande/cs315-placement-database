﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="Mission.aspx.vb" Inherits="Mission" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p style="background-color: #FFFFFF; font-family: 'OLd English Text MT'; font-size: xx-large; font-weight: 500; color: #000000;">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        OUR MISSION</p>
    <p style="background-color: #FFFFFF; font-family: 'Comic Sans MS'; font-size: large; font-weight: 500; color: #000000;">
        <br />
        <br />
        <asp:Image ID="Image1" runat="server" Height="376px" 
            ImageUrl="~/PLACEMENT IMAGES/what_we_belive_in.jpg" />
        <br />
        <br />
        a. Students. We conducted a survey among engineering students who had completed 
        their twelve-month placement by August 2007 to determine their views on the 
        support provided by OPUS throughout their placement experience. Figure 1 
        presents their views on the value of OPUS in obtaining their placement and 
        Figure 2 represents the most useful features during their placement period. The 
        full analysis may be obtained in the paper by Laird and Turner (2008b). 
        Additional comments from students give expression to their views, as follows: 
        ‘The OPUS software proved to be vital in the placement process; it is hard to 
        imagine how placement was organised without the system’. (Mr Colm Higgins; 
        Engineering student) ‘I found this OPUS system very useful - it had a lot of 
        available industrial positions which could<br />
        be checked and applied for, the ability to track my applications was also 
        helpful and being able to automatically attach my CV to positions posted was 
        very easy’. (Charlotte Ann Donnelly; Engineering Management student) ‘Throughout 
        the placement I found the OPUS website extremely user-friendly, the CV builder 
        was well structured and very informative’. (Mr Simon Kernohan; Engineering 
        student) ‘This position was advertised on OPUS. OPUS was of great help to me 
        finding placement. I don’t know how everyone else manages without it’. (Miss 
        Rachel Woods; Technology with Design student)<br />
        Placement Providers. In 2001 on migrating from the old methods of managing 
        placements to OPUS we found that no companies had reservations about using the 
        new system, in fact, many welcomed a ‘modern’ way of doing business. We have 
        found that it has not been necessary to have any training or induction for new 
        users, except for a brief telephone explanation on occa-sion. Some of the 
        comments we have obtained over the years are as follows: ‘The benefits of an 
        interactive system allowing instantaneous updates to all parties has led to a 
        more efficient recruitment process. With a single update, I can communicate to 
        all involved, both students and placement coordinators and it has saved our 
        organisation significant time and effort. The simplification of the student 
        recruitment process has meant that I am much more likely to seek to employ 
        students again from the University of Ulster’. (Mrs Deirdre Francis; Director, 
        Mindready Solutions NI Ltd) ‘The system [OPUS] allows us to review applicant 
        information in real time which allows us to action these CV&#39;s as they arrive. 
        Overall we have found that this has assisted us in significantly reducing 
        recruitment timelines’. (Mr Michael Dawson; HR Services Manager, Schrader 
        Electronics) ‘As a user of the OPUS system, I have found it useful to advertise 
        and attract graduates for job opportunities such as industrial placements as 
        well as other job roles. The system is easy to use<br />
        and the fact that students can download the application form, complete it and 
        return means it’s a more efficient way of processing applications. (Ms Rosemary 
        Breen; HR Manager, TEREX Finlay) ‘The expertise and assistance in promoting 
        placement opportunities within Littelfuse Ireland has been exceptional. The 
        online and interactive nature of OPUS has proved an invaluable resource in 
        placing students quickly and effectively’. (Mrs Caroline Fennessy; HR Manager, 
        Littlefuse Ireland)<br />
        <br />
        <asp:Image ID="Image2" runat="server" Height="262px" 
            ImageUrl="~/PLACEMENT IMAGES/CAREER.jpg" />
        <br />
        <br />
    <br />
</p>
</asp:Content>

