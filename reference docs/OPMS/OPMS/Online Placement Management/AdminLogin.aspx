﻿<%@ Page Language="VB" MasterPageFile="~/OPMS.master" AutoEventWireup="false" CodeFile="AdminLogin.aspx.vb" Inherits="AdminLogin" title="Untitled Page" %>

<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="ContentPlaceHolder1">
        
        
	<div id="main" style="background-color: #FFFFFF; width: 472px;">
		<div id="welcome">
			<h2>Welcome To Online Placement Management!
                            
                            
                            
                            </h2>
			<p><strong> OPMS </strong> The use of the Internet and the World Wide Web have 
                revolutionised the provision of information and the facility for the user to 
                take action on the information obtained. An early example of user ena-bling was 
                the online shopping features within some web sites. These examples prompted us 
                in the School of Engineering at the University of Ulster to explore the use of 
                the Internet to enable students and companies to manage the placement process 
                with the active involvement of the Industrial Place-ment Coordinator. This led 
                to a unique web-based placement management system developed specifi-cally by the 
                placements practitioner and the software programmer to become OPUS (Online 
                Placement University System) since autumn 2000. The basic system was able to 
                manage the first cohort of engi-neering students into placement in 2002. Since 
                these early basics the programme has been developed continuously as an open 
                source product, now at version 4, and being implemented in several universi-ties 
                in the United Kingdom and under investigation by others worldwide.</em></p>
			<p>An unordered list example:</p>
			<ul>
				<li>List item number one</li>
				<li>List item number two</li>
				<li>List item number three </li>
			</ul>
		</div>
		<div id="example">
			<h2>I. INTRODUCTION</h2>
			<p>OPUS, like many other placement management web sites, provides information on 
                placement pro-viders and the placements they offer so that students may view and 
                assess their opportunities. Indeed, all higher education institutions (HEIs) 
                have well-developed web sites to inform their students of va-cancies and how to 
                prepare for their work-integrated learning experience. However, relatively few 
                have developed the means to enable the student, the provider and the placements 
                coordinator (with others) to take effective actions on the web as a follow-on 
                from the information they have viewed. It is too often the case that information 
                must be acted upon by wholly separate means away from the web. Changes and 
                updates are laborious for staff and are at risk of being late. With OPUS the 
                user may control their information, the provider may promote their 
                opportunities, the student can take action to apply for any vacancy and the 
                placements coordinator may control all features. Being on the web, all 
                information may be accessed from anywhere at anytime.</p>
			   <h3> II. PRINCIPLES SUPPORTED BY OPMS</h3>
			<p>From the earliest development we realized that, to be effective in replacing 
                existing complex sys-tems, OPUS needed to support several principles associated 
                with a web-based application and the con-duct of work-integrated learning in 
                compliance with the United Kingdom Quality Assurance Agency Code of Practice, 
                Section 9 (the Code) (2007). Further, several other principles were necessary to 
                commend OPUS to work-based learning practitioners and IT support personnel for 
                their future opera-tions. These principles are developed below..</p>
			<p>An ordered list example:		<li>List item number one</li>
				<li>List item number two</li>
				<li>List item number thre</li>
			</p>
		</div>
	</div>
	<div id="sidebar">
		<div id="login" class="boxed">
			<h2 class="title">Admin Account</h2>
			<div class="content" style="height: 336px">
				<form id="form2" method="post" action="#">
					<fieldset style="height: 298px">
					<legend>Sign-In</legend>
					<label for="inputtext1">Client ID:</label>
					<asp:TextBox ID="TextBox1" runat="server" Height="22px" Width="166px"></asp:TextBox>
					<label for="inputtext2">Password:</label>
					<asp:TextBox ID="TextBox2" runat="server" Height="22px" Width="166px" 
                            TextMode="Password"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="Button1" runat="server" Text="SIGN IN" /><asp:Label 
                            ID="Label1" runat="server"></asp:Label>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButton1" runat="server">Change Password</asp:LinkButton>
                        <br />
                        <br />
                        <asp:Label ID="Label2" runat="server" Text="Enter Old Password"></asp:Label>
                        <br />
                        <asp:TextBox ID="TextBox3" runat="server" Width="165px" TextMode="Password"></asp:TextBox>
                        <br />
                        <asp:Label ID="Label3" runat="server" Text="Enter New Password"></asp:Label>
                        <br />
                        <asp:TextBox ID="TextBox4" runat="server" TextMode="Password" Width="165px"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="Button2" runat="server" Text="UPDATE" Width="163px" />
                        <br />
                        
                        <br />
                        &nbsp;&nbsp;</fieldset>
				</form>
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title">Recent Updates</h2>
			<div class="content">
				<ul>
					<li>
						<h3></h3>
						<p><a href="#"></a></p>
					</li>
					<li>
						<h3></h3>
						<p><a href="#"></a></p>
					</li>
					<li>
						<h3></h3>
						<p><a href="#"></a></p>
					</li>
					<li>
						<h3></h3>
						<p><a href="#"></a></p>
					</li>
				</ul>
			</div>
		</div>
		<div id="partners" class="boxed">
			<h2 class="title">Partners</h2>
			<div class="content">
				<ul>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
					<li><a href="#"></a></li>
				</ul>
			</div>
		</div>
	</div>
	
</asp:Content>


