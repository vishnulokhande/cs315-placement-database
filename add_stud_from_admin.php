  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PAS</title>
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
    </head>



<?php

if(isset($_POST['student_add']))
{  
     
    
        require('mysqli_connect.php');
        $sql = "INSERT INTO Student (roll_no,name,cgpa,dept,location,interests,des_package) VALUES (?,?,?,?,?,?,?)"; 
        $st = $dbc->prepare($sql);
        $st->bind_param('ssdsssd',trim($_POST['rollno']),trim($_POST['name']),trim($_POST['cgpa']),trim($_POST['dept']),trim($_POST['location']),trim($_POST['interests']),trim($_POST['des_package']));

        $st->execute();

        // $message = "Student added";
        //   echo "<script>alert('".$message."'); window.location.href='/cs315/add_stud_from_admin.php';</script>";

        $st->close();
        



        $sql2 = "INSERT INTO SLogin (roll_no,passwrd) VALUES (?,?)"; 
        $st2 = $dbc->prepare($sql2);
        $st2->bind_param('ss',trim($_POST['rollno']),trim($_POST['passwrd']));
        $st2->execute();

        $message = "Student added";
          echo "<script>alert('".$message."'); window.location.href='/cs315/admin_home.php';</script>";

        $st->close();


}

?>


    <body>
      <div class = "part1">
      <form action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']); 
            ?>" method="post">
    

        <fieldset>

          <legend><span class="number">1</span>Set Username and Password</legend>
          <label for="mail">Student Username (use roll no.):</label>
          <input type="text" id="rollno" name="rollno" >

          <label for="name">Student Password:</label>
          <input type="password" id="passwrd" name="passwrd">

          <legend><span class="number">2</span>Fill additional attributes</legend>
          <label for="name">Name:</label>
          <input type="text" id="name" name="name">
     
          <label for="mail">CGPA:</label>
          <input type="text" id="cgpa" name="cgpa">
          
          <label for="mail">Dept:</label>
          <input type="text" id="dept" name="dept">
          
          <label for="name">Desired Location:</label>
          <input type="text" id="location" name="location">

          <label for="name">Student interests areas:</label>
          <input type="text" id="interests" name="interests">

          <label for="name">Student's desired package:</label>
          <input type="text" id="des_package" name="des_package">  

        <!--   <label>Age:</label>
          <input type="radio" id="under_13" value="under_13" name="user_age"><label for="under_13" class="light">Under 13</label><br>
          <input type="radio" id="over_13" value="over_13" name="user_age"><label for="over_13" class="light">13 or older</label> -->
        </fieldset>
        <button type='submit' name='student_add'>Add Student</button> 
        <!-- <button type="submit" name="Update_info">Update</button> -->
      </form>
      </div>
    </body>
</html>