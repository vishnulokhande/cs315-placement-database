<html>
<head>
	<title> Add Candidate Information</title>
</head>
<body>

<?php 

if(isset($_POST['submit']))
{
	$data_missing=array();

	if(empty($_POST['name'])) 
	{
		$data_missing[]='Name';
	}
	else
	{
		$name= trim($_POST['name']);
	}

	if(empty($_POST['rollno'])) 
	{
		$data_missing[]='rollno';
	}
	else
	{
		$rollno= trim($_POST['rollno']);
	}

	if(empty($_POST['emailID'])) 
	{
		$data_missing[]='emailID';
	}
	else
	{
		$emailID= trim($_POST['emailID']);
	}

	if(empty($_POST['mobile'])) 
	{
		$data_missing[]='mobile';
	}
	else
	{
		$mobile= trim($_POST['mobile']);
	}

	if(empty($data_missing))
	{
        echo 'All data entered<br />'; 
        require_once('mysqli_connect.php');
        
        $query = "INSERT INTO candidate (name, rollno, emailID , mobile) VALUES (?,?,?,?)";        
        $stmt = mysqli_prepare($dbc, $query); //statement
        
        // i Integers
        // d Doubles
        // b Blobs
        // s Everything Else
        
        mysqli_stmt_bind_param($stmt, "siss",$name,$rollno, $emailID, $mobile);        
        mysqli_stmt_execute($stmt);        
        $affected_rows = mysqli_stmt_affected_rows($stmt);
        
        if($affected_rows == 1)
        {
            
            echo 'Candidate Entered';            
            mysqli_stmt_close($stmt);            
            mysqli_close($dbc);
            
        } 
        else 
        {	            
            echo 'Error Occurred blah<br />';
            echo mysqli_error($dbc);            
            mysqli_stmt_close($stmt);            
            mysqli_close($dbc);            
        }


    } 
    else
    {
    	echo 'You need to enter the following data<br />';     
	    foreach($data_missing as $missing)
	    {	             
	            echo "$missing<br />";	             
	    }
    }

}

?>
</body>
</html>