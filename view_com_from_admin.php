
<?php
require('admin_tester.php');
?>

  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PAS</title>
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
    </head>

<style>
.button {
     text-align: center;
    text-decoration: none;
    display: inline-block;
 
}

a {
  color: inherit;
    text-decoration: none;
}


</style>



<?php


session_start();
// echo "hello";
if(isset($_GET['view']))
{

  
  error_reporting(E_ALL);
  require_once('mysqli_connect.php');

  $st = $dbc->prepare("SELECT * FROM Company as C where C.com_id = ? ");
  $st->bind_param('s',$_GET['cid']);
  $st->execute();
  $st->bind_result($rn,$nm,$cg,$descptn,$depmt,$location,$interests,$package);
  $row = $st->fetch();
  $st->close();
  $dbc->close();

}

elseif(isset($_POST['company_update']))
{  
     require_once('mysqli_connect.php');
        // var_dump($_SESSION['job']);

       $sql = "UPDATE Company SET c_name=?, cgpa=?, descptn=?, depmt=?,location=?,interests=?,package=? WHERE com_id=?";

        $st = $dbc->prepare($sql);

        $st->bind_param('sdssssds',trim($_POST['name']), trim($_POST['cgpa']), trim($_POST['descptn']),trim($_POST['depmt']),trim($_POST['location']),trim($_POST['interests']),trim($_POST['package']),trim($_POST['rollno']));
        $st->execute();


        // /if($st->fetch())
        // {
          $message = "Details updated in Company";
          echo "<script>alert('".$message."'); window.location.href='/cs315/admin_home.php';</script>";
        // }
        // else
        // {
        //   $message = "Could not uodate details in Compnay";
        //   echo "<script>alert('".$message."'); window.location.href='/cs315/admin_home.php';</script>";
        // }  
        $st->close();

}

elseif(isset($_POST['company_delete']))
{  
     require_once('mysqli_connect.php');
             // var_dump($_SESSION['job']);

       $sql = "DELETE FROM Company WHERE com_id=?";
       $st = $dbc->prepare($sql);
       $st-> bind_param('s',trim($_POST['rollno']));
       $st->execute();


        // if(mysqli_query($dbc, $sql)))
        // {
          $message = " Company deleted";
          echo "<script>alert('".$message."'); window.location.href='/cs315/admin_home.php';</script>";
        // }
        // else
        // {
        //   $message = "Could not delete student details";
        //  echo "<script>alert('".$message."'); window.location.href='/cs315/admin_home.php';</script>";
        // }  
        $st->close();

}





?>


    <body>
      <div class = "part1">
      <form action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']); 
            ?>" method="post">

 <fieldset>
          <legend><span class="number">1</span>Company Information</legend>

          <label for="mail">ID:</label>
          <input type="text" id="rollno" name="rollno" value=<?php echo $rn;?> readonly >

          <label for="name">Company Name:</label>
          <input type="text" id="name" name="name" value=<?php echo $nm;?> >
          
          <label for="mail">Description:</label>
          <input type="text" id="descptn" name="descptn" value=<?php echo "'".$descptn."'";?> >

          <label for="mail">Location :</label>
          <input type="text" id="location" name="location" value=<?php echo $location;?> >
          
          <label for="mail">Package in LPA:</label>
          <input type="text" id="package" name="package" value=<?php echo $package;?> >

          <legend><span class="number">2</span>Company Requirements</legend>  

          <label for="mail">Interests:</label>
          <input type="text" id="Interests" name="interests" value=<?php echo $interests;?> >

          <label for="mail">CGPA:</label>
          <input type="text" id="cgpa" name="cgpa" value=<?php echo $cg;?> >

          <label for="mail">Department:</label>
          <input type="text" id="depmt" name="depmt" value=<?php echo "'".$depmt."'";?> >


          <!--   <label>Age:</label>
          <input type="radio" id="under_13" value="under_13" name="user_age"><label for="under_13" class="light">Under 13</label><br>
          <input type="radio" id="over_13" value="over_13" name="user_age"><label for="over_13" class="light">13 or older</label> -->
        </fieldset>

        <div >
          <button class ='button' type='submit' name='company_delete' >Delete</button>  
          <button class ='button' type='submit' name='company_update' >Update</button>
          </div>
        <!-- <button type="submit" name="Update_info">Update</button> -->
      </form>
      </div>
    </body>
</html>