

<?php
require('stu_tester.php');
?>

  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PAS</title>
        <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/main.css">
    </head>



<?php
    session_start();
    error_reporting(E_ALL);
    require('mysqli_connect.php');

    $st = $dbc->prepare("SELECT * FROM Student as S where S.roll_no = ?");
    $st->bind_param('s',$_SESSION['username']);
    $st->execute();
    $st->bind_result($rn,$nm,$cg,$dp,$location,$interests,$des_package);

    $row = $st->fetch();
    $st->close();
    $dbc->close();
?>


    <body>
    <div style="height: 2%;"></div>
      <div class = "part1">
      <form action = "update.php" method="post">
    

        <fieldset>
          <legend><span class="number">1</span>Your basic info</legend>
          <label for="name">Name:</label>
          <input type="text" id="name" name="name" value=<?php echo $nm;?> >
          

          <label for="mail">Roll Number:</label>
          <input type="text" id="rollno" name="rollno" value=<?php echo $rn;?> readonly>
          
           <label for="mail">CGPA:</label>
          <input type="text" id="cgpa" name="CGPA" value=<?php echo $cg;?> >
          

           <label for="mail">Dept:</label>
          <input type="text" id="dept" name="Department" value=<?php echo $dp;?> >
          
          <legend><span class="number">2</span>Your Requirements</legend>
          <label for="name">Desired Location:</label>
          <input type="text" id="location" name="location" value=<?php echo $location;?> >

          <label for="name">What are your interests?:</label>
          <input type="text" id="interests" name="interests" value=<?php echo "'".$interests."'";?> >

          <label for="name">What is your desired package?:</label>
          <input type="text" id="des_package" name="des_package" value=<?php echo $des_package;?> >  


          
        <!--   <label>Age:</label>
          <input type="radio" id="under_13" value="under_13" name="user_age"><label for="under_13" class="light">Under 13</label><br>
          <input type="radio" id="over_13" value="over_13" name="user_age"><label for="over_13" class="light">13 or older</label> -->
        </fieldset>

        <button type="submit" name="Update_info">Update</button>
      </form>
      </div>
    </body>
</html>