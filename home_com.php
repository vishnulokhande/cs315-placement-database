
<?php
require('com_tester.php');
?>


<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Responsive CSS Tabs</title>
   
    
    
        <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
@import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html, body {
  height: 100%;
}

body {
  font: 14px/1 'Open Sans', sans-serif;
  color: #555;
  background: #eee;
}

h1 {
  padding: 50px 0;
  font-weight: 400;
  text-align: center;
}

p {
  margin: 1% 1% 1%;
  line-height: 1.5;
}

main {
  min-width: 320px;
  max-width: 80%;
  padding: 50px;
  margin: 0 auto;
  background: #fff;
}

section {
  display: none;
  padding: 20px 0 0;
  border-top: 1px solid #ddd;
   background-color: #fff;
}

input {
  display: none;
}

label {
  display: inline-block;
  margin: 0 0 -1px;
  padding: 15px 18px;
  font-weight: 600;
  text-align: center;
  color: #bbb;
  border: 1px solid transparent;
}

a {
    text-decoration: none;
}



label:before {
  font-family: fontawesome;
  font-weight: normal;
  margin-right: 10px;
}

label[for*='1']:before {
  content: '\f1cb';
}

label[for*='2']:before {
  content: '\f17d';
}

label[for*='3']:before {
  content: '\f16b';
}

label[for*='4']:before {
  content: '\f1a9';
}
label[for*='5']:before {
  content: '\231a';
}
label[for*='6']:before {
  content: '\281a';
}
label[for*='7']:before {
  content: '\341a';
}

label:hover {
  color: #888;
  cursor: pointer;
}

input:checked + label {
  color: #555;
  border: 1px solid #ddd;
  border-top: 2px solid orange;
  border-bottom: 1px solid #fff;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4,
#tab5:checked ~ #content5,
#tab6:checked ~ #content6{
  display: block;
}

#content6{

margin-left: 10%;


}



.w3-card{
  width: 90%;
   margin-bottom: 1%;
   
   margin-left: 2%;
  padding-top: 2%;
  padding-left: 2%;
  padding-right: 2%;
  padding-bottom: 1%;
  box-shadow:0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important
}





@media screen and (max-width: 650px) {
  label {
    font-size: 0;
  }

  label:before {
    margin: 0;
    font-size: 18px;
  }
}
@media screen and (max-width: 400px) {
  label {
    padding: 15px;
  }
}


    </style>

    
        <script src="js/prefixfree.min.js"></script>

   

    
  </head>



 






  <body>

    <h1>Placement Automation System</h1>

<main>
  
  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1">Home</label>
    
  <input id="tab2" type="radio" name="tabs" >
  <label for="tab2">Update Profile</label>
    
  <input id="tab3" type="radio" name="tabs" >
  <label for="tab3">Make Student Post</label>
  
  <input id="tab4" type="radio" name="tabs" >
  <label for="tab4">Message Admin</label>


  <input id="tab5" type="radio" name="tabs" >
  <label for="tab5">Student Applications</label>

    <input id="tab6" type="radio" name="tabs" >
  <label for="tab6">Calender</label>

  <input id="tab7" type="submit" name="tabs" onclick="location.href='logout.php';">
  <label for="tab7">Logout</label> 


<?php
    
    session_start();

    

//   $("#tab5").click(function(){






// });
?>
    
  <section id="content1">
   


    <?php

    
  
      // echo "raghav is great";
      // echo $_SESSION['username'];
     require('mysqli_connect.php');
     $result = mysqli_query($dbc, "SELECT * FROM C_posts as S order by S.dte desc");
//   //   echo "SELECT * FROM student_apply as SA where SA.com_id = '".$_SESSION['username']."' ";
  // $st = $dbc->prepare("SELECT * FROM student_apply ");
  // $st->bind_param('s',$_SESSION['username']);
  // $st->execute();
  // $st->bind_result($rn,$ci,$st);
  // var_dump($rn);


  // echo mysqli_num_rows($result);


  while ($row = $result->fetch_array())
    {
     
     echo "<div class='w3-card'>";
     echo "<p>".$row[0]."</p>";
     echo "<p align='right'><font size='1'> ".$row[1]."</font> </p>";
     echo "</div>";

     
      
    }

//   // $st->close();



  //   }
    $result->close();
  $dbc->close();

// echo $row;  

//   // $st->close();







?>









  </section>
    
  <section id="content2">

<?php 
    include("update_com_info.php");
 ?>


  </section>
    
  <section id="content3">
    

  <?php include("makepost_com.php"); ?>


 </section>

<section id="content4">

      <?php include("msg_admin.php"); ?>




  </section>


 
    
  <section id="content5">

      <?php include("con_tent.php"); ?>




  </section>





  </section>
    <section id="content6">    
      <iframe src="https://calendar.google.com/calendar/embed?src=sdrsbg2b56l5r8f2fpd7q9rl68%40group.calendar.google.com&ctz=Asia/Calcutta" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
  </section>
    
</main>
    
    
    
    
    
  </body>
</html>